int statusFlag = 0;

#include <Wire.h>

// Cooking API libraries
#include <arduinoUART.h>
#include <arduinoUtils.h>

// Sigfox library
#include <arduinoSigfox.h>

// Pin definition for Sigfox module error LED:

//////////////////////////////////////////////
uint8_t socket = SOCKET0;  
//uint8_t socket1 = SOCKET1;
//////////////////////////////////////////////
uint8_t error;

void setup() {
  // put your setup code here, to run once:
  pinMode(13,OUTPUT);
}

void loop() {
  error = Sigfox.ON(socket);
digitalWrite(13,HIGH);
  error = Sigfox.send((char *)"4f4e");
  delay(5000);
  digitalWrite(13,LOW);
  delay(5000);
  error = Sigfox.send((char *)"4f4f");
  delay(5000);
  digitalWrite(13,LOW);
  delay(5000);
  
}
