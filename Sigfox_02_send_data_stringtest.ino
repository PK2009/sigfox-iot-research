
#include <Wire.h>

// Cooking API libraries
#include <arduinoUART.h>
#include <arduinoUtils.h>

// Sigfox library
#include <arduinoSigfox.h>

// Pin definition for Sigfox module error LED:
const int error_led =  13;

//////////////////////////////////////////////
uint8_t socket = SOCKET0;     //Asign to UART0
//////////////////////////////////////////////

uint8_t error;
//uint8_t check;


void setup()
{
  pinMode(error_led, OUTPUT);
//  test = Serial.read();
 
 
}


void loop() 
{  
 int statusFlag=0;
  //////////////////////////////////////////////
  // 1. switch on
  //////////////////////////////////////////////
  error = Sigfox.ON(socket);
  //check = Sigfox.ON(socket);

  // Check status
  if( error == 0 )
  {
    //"Switch ON OK"
    digitalWrite(error_led, LOW);
  }
  else
  {
    //"Switch ON ERROR"
    digitalWrite(error_led, HIGH);
  }

  
  //////////////////////////////////////////////
  // 2. send data
  //////////////////////////////////////////////

  // Send 12 bytes at most
  if(statusFlag == 0){
    error = Sigfox.send((char *)"6x6e");
    statusFlag = 1;
  }else if (statusFlag == 1){
    error = Sigfox.send((char *)"4f4e");
  }

  // Check sending status
  if( error == 0 )
  {
    //"Sigfox packet sent OK"
    digitalWrite(error_led, LOW);
  }
  else
  {
    //"Sigfox packet sent ERROR"
    digitalWrite(error_led, HIGH);
  }

  //////////////////////////////////////////////
  // 3. sleep
  //////////////////////////////////////////////
  delay(1000);
}
