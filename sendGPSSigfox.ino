#include <Adafruit_GPS.h>   //Install the Adafruit GPS Library
#include <SoftwareSerial.h>

#include <Wire.h>

// Cooking API libraries
#include <arduinoUART.h>
#include <arduinoUtils.h>

// Sigfox library
#include <arduinoSigfox.h>

// Pin definition for Sigfox module error LED:

//////////////////////////////////////////////
uint8_t socket = SOCKET0;     //Asign to UART0
//////////////////////////////////////////////

uint8_t error;
uint8_t data[12];
uint8_t size;
  char charBuff[10];
  char charBuff2[10];


//Load the Software Serial Library

SoftwareSerial mySerial(3,2); // Initailaise the Software Serial ports
Adafruit_GPS GPS_Tracker(&mySerial);  //Create the GPS Object

String NMEA1 ; //variable for first NMEA sentence
String NMEA2 ; //variable for second NMEA sentence
char c; //To read characters coming from the GPS

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); //Turn on serial monitor
  GPS_Tracker.begin(9600); //Turn on GPS as 9600 baud 
  GPS_Tracker.sendCommand("$PGCMD,33,0*6D"); //Turn on antenna update data 
  GPS_Tracker.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ); //Set update reate to 10 Hz
  GPS_Tracker.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA); //Request RMC and GGA sentences only
  delay(1000);
    error = Sigfox.ON(socket);

  }

void readGPS() {

  clearGPS();
  while(!GPS_Tracker.newNMEAreceived()){ //loop until you have a good NMEA sentence
    c=GPS_Tracker.read();    
    }
  GPS_Tracker.parse(GPS_Tracker.lastNMEA()); // Parse that last good NMEA sentence
  NMEA1=GPS_Tracker.lastNMEA(); 

  while(!GPS_Tracker.newNMEAreceived()){ //loop until you have a good NMEA sentence
    c=GPS_Tracker.read();    
    }
  GPS_Tracker.parse(GPS_Tracker.lastNMEA()); // Parse that last good NMEA sentence
  NMEA2=GPS_Tracker.lastNMEA(); 
   NMEA1.toCharArray(charBuff, 10);
  NMEA2.toCharArray(charBuff2, 10);
}

void clearGPS(){ //clear old and currupt data from serial port
  while(!GPS_Tracker.newNMEAreceived()){ //loop until you have a good NMEA sentence
    c=GPS_Tracker.read();    
    }
  GPS_Tracker.parse(GPS_Tracker.lastNMEA()); // Parse that last good NMEA sentence
  
  while(!GPS_Tracker.newNMEAreceived()){ //loop until you have a good NMEA sentence
    c=GPS_Tracker.read();    
    }
  GPS_Tracker.parse(GPS_Tracker.lastNMEA()); // Parse that last good NMEA sentence
}

void loop() {
    //readGPS();
    //data[0] = charBuff[1];
    //data[1] = charBuff[0];
    data[0] = 'a';
    data[1] = 'c';
    size=4;
    Serial.println(charBuff);
  delay(10000);
  //error = Sigfox.send(charBuff);
  error = Sigfox.send(data,size);
  delay(7000);
  // put your main code here, to run repeatedly:

     // Serial.println("----------------------------------------------------------------------------------");
      //Serial.println(NMEA1);
      //Serial.println(NMEA2);
      //Serial.println("----------------------------------------------------------------------------------");  
  Serial.println("chekc send");
 
  if(GPS_Tracker.fix == 1){ //Only log data after you have a FIX
      Serial.println("----------------------------------------------------------------------------------");
      Serial.println(NMEA1);
      Serial.println(NMEA2);
      Serial.println("----------------------------------------------------------------------------------");
      Serial.print("Latitude : ");
      Serial.println(GPS_Tracker.latitude,4);
      Serial.print("Longitude : ");
      Serial.println(GPS_Tracker.longitude,4);
      Serial.print("Altitude : ");
      Serial.println(GPS_Tracker.altitude);
      Serial.print("Latitude Hemisphere : "); // N or S
      Serial.println(GPS_Tracker.lat);
      Serial.print("Longitude Hemisphere : "); // E or W
      Serial.println(GPS_Tracker.lon);
      Serial.println(" + ----- End of GPS data packet ! ----- + ");

    }
    
}
